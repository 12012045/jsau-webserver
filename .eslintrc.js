module.exports ={
    "env": {
        "browser": true,
        "es2021": true,
        "jest": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "script"
    },
    "ignorePatterns": [
        "dist/",
        "webpack.*.js",
        "*/.json",
        "*/.css",
        "*/.html"
    ],
    "rules": {}
}