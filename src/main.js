const express = require("express")
const bodyParser = require("body-parser")
const morgan = require("morgan")
const path = require("path")
const fs = require("fs")
const fsp = require("fs/promises")
const cookieParser = require("cookie-parser")
const emojiParser = require("@12012045/emoticons-to-emojis/src/main")

const app = express()

app.set("view engine", "ejs")
app.set("views", path.join(__dirname, "views"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, "/public")))
app.use(morgan("dev"))
app.use(cookieParser())

const DBPATH = path.join(__dirname, "db.json")

function Db() {
  this.messages = []
  this.users = []
  this.discussions = []
}
function test(){
  const inputText = 'Hello :) How are you? :D';
  const convertedText = emojiParser(inputText);
  console.log(inputText + '|' + convertedText)
}
test()
async function getDB() {
  if (!fs.existsSync(DBPATH)) {
    // we create the database
    await fsp.open(DBPATH, "w+")

    // we intialize the database
    const dbInit = JSON.stringify(new Db())
    await fsp.writeFile(DBPATH, dbInit)
  }
  let DB = await fsp.readFile(DBPATH, "utf8")
  await fsp.filehandle?.close()  
  return JSON.parse(DB)
}

async function updateDB(dbObject) {
  await fsp.writeFile(DBPATH, JSON.stringify(dbObject))
  await fsp.filehandle?.close()  
}

// middleware to test if authenticated
async function validateCookie(req, res, next) {
  const { cookies } = req
  let DB = await getDB()
  if ("user" in cookies) {
    console.log("existing cookie")
    if (DB.users.findIndex((user) => user.pseudo == cookies.user) != -1) {
      next()
    } else res.status(403).json({ message: "not logged" })
  } else res.status(403).json({ message: "not logged" })
}
function restrictLogin(req, res, next) {
  const { cookies } = req
  if ("user" in cookies && (req.path == "/" || req.path == "/login"))
    res
      .status(403)
      .json({ message: "you are already logged in, clear cookies or logout" })
  else next()
}

async function getDiscussions(user) {
  let DB = await getDB()
  let loggedUser = user
  let discussions = []
  if (DB.discussions.length > 0) {
    DB.discussions.forEach((d) => {
      let isUserMemberOfDiscussion =
        d.members.findIndex((member) => member == loggedUser) != -1
      if (isUserMemberOfDiscussion) {
        let newDiscussion = {}
        newDiscussion.discussionId = d.discussionId
        newDiscussion.members = d.members
        newDiscussion.messages = []
        if (DB.messages.length > 0) {
          DB.messages.forEach((m) => {
            if (m.discussionId == d.discussionId) newDiscussion.messages.push(m)
          })
        }
        discussions.push(newDiscussion)
      }
    })
  }
  return discussions
}

app.get(["/", "/login"], restrictLogin, async (req, res) => {
  res.render("login", {})
})

app.post("/login", async function (req, res) {
  let DB = await getDB()

  let isNewUser = false

  if (DB.users.length > 0) {
    isNewUser = DB.users.findIndex((user) => user.pseudo == req.body.user) != -1
  }
  //we create the user if he's not registered
  if (!isNewUser) {
    const newUser = {}
    newUser.pseudo = req.body.user
    newUser.password = "verysecurepassword"
    DB.users.push(newUser)
    await updateDB(DB)
    console.log("creating new user")
  }

  res.cookie("user", req.body.user, { maxAge: 900000, httpOnly: false })
  res.redirect("/discussions")
})

app.post("/logout", function (req, res) {
  res.clearCookie("user")
  res.redirect("/")
})

// post a message in a discussion
app.post("/messages", validateCookie, async (req, res) => {
  let DB = await getDB()

  const { discussionId, senderPseudo, messageContent } = req.body
  const newMessage = {}
  newMessage.messageId = new Date().valueOf()
  newMessage.discussionId = discussionId
  newMessage.senderPseudo = senderPseudo
  newMessage.content = emojiParser(messageContent)
  newMessage.date = new Date()
  let convAndUserExists = false
  let isUserMemberOfDiscussion = false
  if (DB.discussions.length > 0 && DB.users.length > 0) {
    console.log(DB.discussions.length + " : " + DB.users.length)
    convAndUserExists =
      DB.discussions.findIndex(
        (discussion) => discussion.discussionId == discussionId
      ) != -1 && DB.users.findIndex((user) => user.pseudo == senderPseudo) != -1
    const discussionMembers = DB.discussions.find(
      (discussion) => discussion.discussionId == discussionId
    ).members
    isUserMemberOfDiscussion =
      discussionMembers.findIndex((member) => member == senderPseudo) != -1
  }

  if (convAndUserExists && isUserMemberOfDiscussion) {
    DB.messages.push(newMessage)
    await updateDB(DB)

    res.redirect("/discussions")
  } else res.status(400).json({ error: "Données Invalides" })
})

//create a new discussion
app.post("/discussions", validateCookie, async (req, res) => {
  let DB = await getDB()
  const usersList = req.body.participants
  let isUsersValid = true
  usersList.forEach((pseudo) => {
    if (DB.users.length > 0) {
      if (DB.users.findIndex((user) => user.pseudo == pseudo) == -1) {
        isUsersValid = false
      }
    }
  })
  if (isUsersValid) {
    const newConvo = {}
    newConvo.discussionId = new Date().valueOf()
    newConvo.members = req.body.participants
    newConvo.members.push(req.cookies.user)
    DB.discussions.push(newConvo)
    await updateDB(DB)
    return res
      .status(201)
      .json({ message: "Conversation créée", id: newConvo.discussionId })
  } else res.status(400).json({ error: "Données Invalides" })
})

// retrieve all discussions and corresponding messages
app.get("/discussions", validateCookie, async (req, res) => {
  let discussions = await getDiscussions(req.cookies.user)
  const haveDiscussions = discussions.length > 0
  let selectedDiscussion
  if (!req.query.selectedDiscussion && haveDiscussions) {
    selectedDiscussion = discussions[0].discussionId
  } else selectedDiscussion = req.query.selectedDiscussion
  const loggedUser = req.cookies.user
  res.render("home", {
    discussions,
    loggedUser,
    selectedDiscussion,
    haveDiscussions
  })
})

app.get("/newdiscussion", validateCookie, async (req, res) => {
  res.render("add-discussion", { user: req.user })
})

module.exports = app
