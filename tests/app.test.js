const request = require('supertest')
const app = require("../src/main.js") 
const rimraf = require('rimraf')
const path = require('path')

const dbFilePath = path.join(__dirname, '../src/db.json')

describe('WEB SERVER', () => {
  it('should respond with status 200 on GET /login', async () => {
    const response = await request(app).get('/login')
    expect(response.status).toBe(200)
  })
  it('should responds with a redirect status 302 when the user can log in successfully', async () => {
    const response = await request(app)
      .post('/login')
      .send({ user: 'testuser' })
    expect(response.statusCode).toBe(302)
  })
  it('should respond with a redirect status 302 whe the user can log out successfully', async () => {
    const response = await request(app).post('/logout')
    expect(response.statusCode).toBe(302)
  })
  
  it('should respond with a status 403 when non logged user try to send a message',async()=>{
    const response = await request(app)
    .post('/messages')
    .send( { discussionId : -1, senderPseudo:"someone", messageContent:"somethingg" } )
    expect(response.statusCode).toBe(403)
  })
  
  it('should respond with a status 403 when non logged user try to create a discussion',async()=>{
    const response = await request(app)
    .post('/discussions')
    .send( { usersList :["someone","nobody"] } )
    expect(response.statusCode).toBe(403)
  })

  //to refactor
  it('should respond with a status 201 after one existing user try to create a discussion with another existing user, and with a status 302 when message is send in that discussion',async()=>{
    //log in with user1
    const login_response = await request(app)
    .post('/login')
    .send({ user: 'testuser' })
    expect(login_response.statusCode).toBe(302)
    
    //log out with user1
    const logout_response = await request(app).post('/logout')
    expect(logout_response.statusCode).toBe(302)
    //log in with user2
    const login_response2 = await request(app)
    .post('/login')
    .send({ user: 'testuser2' })
    expect(login_response2.statusCode).toBe(302)

    const userCredentials = {
      user: 'testuser2',
    }
    //create discussion between testuser1 and testuser2
     const discussion_response = await request(app)
    .post('/discussions')
    .set('Cookie', [`user=${userCredentials.user}`]) 
    .send({ participants: ['testuser'] })

    expect(discussion_response.statusCode).toBe(201) 
     const  id = discussion_response.body.id

     //send a message in the new discussion
     const response = await request(app)
     .post('/messages')
     .set('Cookie', [`user=${userCredentials.user}`])
     .send( { discussionId : id, senderPseudo:"testuser2", messageContent:"hello test user1" } )
     expect(response.statusCode).toBe(302)
     console.log(response.headers)

  })
})
afterAll(() => {
  rimraf.sync(dbFilePath)    
})
